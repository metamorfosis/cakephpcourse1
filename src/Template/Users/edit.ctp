<div class="row">
    <div class="col-xs-12 col-md-6 col-md-offset-3">
        <div class="page-header">
            <h2>Editar Usuario</h2>
        </div>
        <?= $this->Form->create($user, ['novalidate']) ?>
        <?= $this->element('Users/fields') ?>
        <?= $this->Form->button(__('Editar'), ['class' => 'btn btn-lg btn-primary']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>