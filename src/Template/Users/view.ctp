<div class="panel panel-default">
    <div class="panel-heading">
        <h2>
            <?= 
                h($user->first_name) . ' ' . h($user->last_name) 
            ?>
        </h2>
    </div>
    <div class="panel-body">
    <p>
        <ul>
            <li>Nombre: <?= h($user->first_name) ?></li>
            <li><?= h($user->last_name) ?></li>
            <li><?= h($user->email) ?></li>
            <li><?= h($user->username) ?></li>
            <li><?= h($user->birthday) ?></li>
            <li>Activo: <?= $user->active ? __('Yes') : __('No'); ?></li>
            <li>Role: <?= $user->role ? __('Usuario') : __('Administrador'); ?></li>
        </ul>
    </p>
    </div>

    <!-- Table -->
    <table class="table table-hover table-reponsive">
        <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Url') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->bookmarks as $bookmarks): ?>
            <tr>
                <td><?= h($bookmarks->id) ?></td>
                <td><?= h($bookmarks->title) ?></td>
                <td><?= h($bookmarks->description) ?></td>
                <td><?= h($bookmarks->url) ?></td>
                <td><?= h($bookmarks->user_id) ?></td>
                <td>
                    <?= $this->Html->link(__('View'), ['controller' => 'Bookmarks', 'action' => 'view', $bookmarks->id],
                        ['class' => 'btn btn-xs btn-info btn-block']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bookmarks', 'action' => 'edit', $bookmarks->id],
                        ['class' => 'btn btn-xs btn-warning btn-block']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bookmarks', 'action' => 'delete', $bookmarks->id], 
                        ['class' => 'btn btn-xs btn-danger btn-block'],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $bookmarks->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
    </table>
</div>