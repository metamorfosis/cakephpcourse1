<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="page-header">
            <h2>Usuarios</h2>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><?= $this->Paginator->sort('id') ?></th>
                        <th><?= $this->Paginator->sort('first_name', ['Nombre']) ?></th>
                        <th><?= $this->Paginator->sort('last_name', ['Apellido']) ?></th>
                        <th><?= $this->Paginator->sort('email', ['Correo']) ?></th>
                        <th><?= $this->Paginator->sort('username', ['Usuario']) ?></th>
                        <th><?= $this->Paginator->sort('birthday', ['Cumpleaños']) ?></th>
                        <th><?= $this->Paginator->sort('created') ?></th>
                        <th><?= $this->Paginator->sort('modified') ?></th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user): ?>
                    <tr>
                        <td><?= $this->Number->format($user->id) ?></td>
                        <td><?= h($user->first_name) ?></td>
                        <td><?= h($user->last_name) ?></td>
                        <td><?= h($user->email) ?></td>
                        <td><?= h($user->username) ?></td>
                        <td><?= h($user->birthday) ?></td>
                        <td><?= h($user->created) ?></td>
                        <td><?= h($user->modified) ?></td>
                        <td>
                            <?= $this->Html->link(__('View'), ['action' => 'view', $user->id],
                                ['class' => 'btn btn-sm btn-info']) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id],
                                ['class' => 'btn btn-sm btn-warning']) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id],
                                ['class' => 'btn btn-sm btn-danger'],
                                ['confirm' => __('Esta seguro de eliminar este usuario # {0}?', $user->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>
</div>