<br>
<div class="container">
    <div class="row">
        <div class="jumbotron">
            <h1>
                Bienvenido,
                <?=
                    $this->Html->link(
                        $currentUser['first_name'] . ' ' . $currentUser['last_name'],
                        [
                            'controller' => 'Users',
                            'action' => 'view',
                            $currentUser['id']
                        ]
                    )
                ?>!
            </h1>
            <p>
                This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.
            </p>
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
        </div>
    </div>
</div>