    <nav class="navbar navbar-inverse nav-users">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?= $this->Html->link('PotCake', ['controller' => 'Users', 'action' => 'index'], ['class' => 'navbar-brand']) ?>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Inicio <span class="sr-only">(current)</span></a></li>
                    <?php if(isset($currentUser)): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php if($currentUser['role'] == 0): ?>
                                <li>
                                    <?= $this->Html->link('Listar', ['controller' => 'Users', 'action' => 'index']) ?>
                                    
                                </li>
                                <li>
                                    <?= $this->Html->link('Crear', ['controller' => 'Users', 'action' => 'add']) ?>
                                    
                                </li>
                                <li>
                                    <?= $this->Html->link('Editar', ['controller' => 'Users', 'action' => 'edit', $currentUser['id']]) ?>
                                    
                                </li>
                            <?php endif; ?>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <?=
                                        $this->Html->link('Perfil',
                                            ['controller' => 'Users', 'action' => 'view', $currentUser['id']
                                        ])
                                    ?>
                                </li>

                            </ul>
                    </li>
                    <?php endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if(isset($currentUser)): ?>
                    <li>
                        <?= 
                            $this->Html->link('Salir', 
                                [
                                    'contoller' => 'Users',
                                    'action' => 'logout'
                                ]) 
                        ?>
                        
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>