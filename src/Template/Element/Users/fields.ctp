<fieldset>
    <?php
    echo $this->Form->input('first_name', ['label' => 'Nombre']);
    echo $this->Form->input('last_name');
    echo $this->Form->input('email');
    echo $this->Form->input('username');
    echo $this->Form->input('password', ['label' =>'Contraseña', 'value' => '']);
    echo $this->Form->input('birthday');
    ?>
</fieldset>