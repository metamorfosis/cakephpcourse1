<?php
use Phinx\Migration\AbstractMigration;

class CreateUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table 
            ->addColumn('first_name', 'string', array('limit' => 100))
            ->addColumn('last_name', 'string', array('limit' => 100))
            ->addColumn('email', 'string', array('limit' => 150))
            ->addColumn('username', 'string', array('limit' => 100))
            ->addColumn('password', 'string', array())
            ->addColumn('birthday', 'datetime', array())
            ->addColumn('role', 'boolean', array())
            ->addColumn('active', 'boolean', array())
            ->addColumn('created', 'datetime', array())
            ->addColumn('modified', 'datetime', array());
        $table->create();
    }
}
