<?php
use Migrations\AbstractMigration;

class CreateBookmarsSeedMigration extends AbstractMigration
{
    public $faker;
    public $populator;

    public function up(){

        $this->faker = \Faker\Factory::create('es_VE');
        $this->populator = new Faker\ORM\CakePHP\Populator($this->faker);

        $this->populator
            ->addEntity(
                'Bookmarks', 400, [
                    'title' => function (){
                        return $this->faker->sentence($nbWords = 3, $variableNbWords = true);
                    },
                    'description' => function (){
                        return $this->faker->paragraph($nbSentences = 3, $variableNbSentences = true);
                    },
                    'url' => function (){
                        return $this->faker->url();
                    },
                    'created' => function (){
                        return $this->faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now');
                    },
                    'modified' => function (){
                        return $this->faker->dateTimeBetween($startDate = 'now', $endDate = 'now');
                    },
                    'user_id' => function (){
                        return rand(1, 101);
                    }
                ]
            );   

            $this->populator->execute();     
    }
}
